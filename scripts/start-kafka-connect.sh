#!/bin/bash -e

export KAFKA_HEAP_OPTS="-Xmx1024M"
exec "/opt/kafka/bin/connect-distributed.sh" "/opt/kafka/config/connect-distributed.properties"
